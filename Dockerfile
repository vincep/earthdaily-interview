FROM ubuntu:latest

ENV DEBIAN_FRONTEND=noninteractive

COPY /cloud-deploy-master/aws/src/bin/* /usr/local/bin/

# Install packages
RUN apt-get update -y && apt-get install -y curl unzip groff jq bc wget zip

# Download and unzip install
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip

# Run AWS install
RUN ./aws/install

# Download Go and Install
RUN wget "https://go.dev/dl/go1.18.linux-amd64.tar.gz"
RUN tar -C /usr/local -xzf go1.18.linux-amd64.tar.gz
ENV PATH="${PATH}:/usr/local/go/bin"
