package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/aws/aws-lambda-go/events"
	runtime "github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-lambda-go/lambdacontext"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/lambda"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/ses"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/nyaruka/phonenumbers"
)

var client = lambda.New(session.New())

func callLambda() (string, error) {
	input := &lambda.GetAccountSettingsInput{}
	req, resp := client.GetAccountSettingsRequest(input)
	err := req.Send()
	output, _ := json.Marshal(resp.AccountUsage)
	return string(output), err
}

type UploadData struct {
	Email       string
	PhoneNumber string
	Message     string
}

func getS3Object(bucket string, key string) UploadData {

	// Local testing params
	// sess, err := session.NewSession(&aws.Config{
	// 	Region:      aws.String("ca-central-1"),
	// 	Credentials: credentials.NewSharedCredentials("", "default"),
	// })

	// if err != nil {
	// 	log.Fatal(err)
	// }

	// _, err = sess.Config.Credentials.Get()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// sess = session.Must(session.NewSessionWithOptions(session.Options{
	// 	SharedConfigState: session.SharedConfigEnable,
	// }))
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

	svc := s3.New(session.New())
	input := &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	}

	result, err := svc.GetObject(input)
	if err != nil {
		fmt.Println(err)
	}
	defer result.Body.Close()
	readBody, err := ioutil.ReadAll(result.Body)
	if err != nil {
		fmt.Println(err)
	}

	var objectData UploadData
	err = json.Unmarshal(readBody, &objectData)
	if err != nil {
		fmt.Println("Error unmarshalling object", err)
	}

	fmt.Println("object uploaded %+v", objectData)

	return objectData

}

func sendSMS(phoneNumber string, message string) {

	// Local TEsting PARAMS
	// sess, err := session.NewSession(&aws.Config{
	// 	Region:      aws.String("ca-central-1"),
	// 	Credentials: credentials.NewSharedCredentials("", "default"),
	// })

	// if err != nil {
	// 	log.Fatal(err)
	// }

	// _, err = sess.Config.Credentials.Get()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// sess = session.Must(session.NewSessionWithOptions(session.Options{
	// 	SharedConfigState: session.SharedConfigEnable,
	// }))
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	svc := sns.New(session.New())

	num, err := phonenumbers.Parse(phoneNumber, "CA")
	if err != nil {
		fmt.Println("Could not parse phonenumber", err)
	}

	input := &sns.PublishInput{
		Message:     aws.String(message),
		PhoneNumber: aws.String(phonenumbers.Format(num, phonenumbers.E164)),
	}

	fmt.Println("SMS Messag format %v, %v", *input.Message, *input.PhoneNumber)
	svc.Publish(input)

}

func sendEmail(emailAddress string, message string) {

	// Local TEsting PARAMS
	// sess, err := session.NewSession(&aws.Config{
	// 	Region:      aws.String("ca-central-1"),
	// 	Credentials: credentials.NewSharedCredentials("", "default"),
	// })

	// if err != nil {
	// 	log.Fatal(err)
	// }

	// _, err = sess.Config.Credentials.Get()
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// sess = session.Must(session.NewSessionWithOptions(session.Options{
	// 	SharedConfigState: session.SharedConfigEnable,
	// }))
	// ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
	svc := ses.New(session.New())

	input := &ses.SendEmailInput{
		Destination: &ses.Destination{
			CcAddresses: []*string{},
			ToAddresses: []*string{
				aws.String(emailAddress),
			},
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Text: &ses.Content{
					Charset: aws.String("UTF-8"),
					Data:    aws.String(message),
				},
			},
			Subject: &ses.Content{
				Charset: aws.String("UTF-8"),
				Data:    aws.String(message),
			},
		},
		Source: aws.String("vincep.sfu+sestesting@gmail.com"),
	}
	_, err := svc.SendEmail(input)

	if err != nil {
		log.Fatal(err)
	}

}

func handleRequest(ctx context.Context, s3Event events.S3Event) (string, error) {
	// event
	eventJson, _ := json.MarshalIndent(s3Event, "", "  ")
	log.Printf("EVENT: %s", eventJson)
	for _, record := range s3Event.Records {
		s3 := record.S3
		fmt.Printf("[%s - %s] Bucket = %s, Key = %s \n", record.EventSource, record.EventTime, s3.Bucket.Name, s3.Object.Key)
		data := getS3Object(s3.Bucket.Name, s3.Object.Key)
		sendSMS(data.PhoneNumber, data.Message)
		sendEmail(data.Email, data.Message)
	}
	// environment variables
	log.Printf("REGION: %s", os.Getenv("AWS_REGION"))
	log.Println("ALL ENV VARS:")
	for _, element := range os.Environ() {
		log.Println(element)
	}
	// request context
	lc, _ := lambdacontext.FromContext(ctx)
	log.Printf("REQUEST ID: %s", lc.AwsRequestID)
	// global variable
	log.Printf("FUNCTION NAME: %s", lambdacontext.FunctionName)
	// context method
	deadline, _ := ctx.Deadline()
	log.Printf("DEADLINE: %s", deadline)
	// AWS SDK call
	usage, err := callLambda()
	if err != nil {
		return "ERROR", err
	}
	return usage, nil
}

func main() {
	runtime.Start(handleRequest)

	// obj := getS3Object("earthdaily-interview-test", "upload2.json")
	// sendSMS(obj.PhoneNumber, obj.Message)
	// sendEmail(obj.Email, obj.Message)
}
