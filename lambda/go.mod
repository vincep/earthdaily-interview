module main

go 1.17

require (
	github.com/aws/aws-lambda-go v1.28.0
	github.com/aws/aws-sdk-go v1.43.19
	github.com/nyaruka/phonenumbers v1.0.74
)

require (
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	golang.org/x/text v0.3.7 // indirect
)
