# Overview

This package creates, deletes, and updates aws resources using goformation and MakeStack. 

 Execution step creates:
  -  the s3Bucket we are uploading to
  -  the uploading user 
  -  the dynamoDb database

 Update step:
   - Adds the user to the uploader group
   - Creates the lambda function

Items that are not managed by this package:
 - Lambda role
 - Uploader group
 - Lambda s3 Bucket
 - Admin user and associate keys



# Instructions

1. export AWS_PROFILE=default && export AWS_REGION="ca-central-1"
2. run `go build`
3. run `./main -a execute`
4. run `./main -a update`
5. Add event trigger to lambda function for s3 object create 
6. Create an access key for the uploader user and put it in env. 
7. Set creds to upload user. 
8. Upload files to s3 bucket by running `./upload.sh` in the upload folder
9. You can demolish the aws stack by running `./main -a delete`


# Lambda Function Update Instructions
1. Simply commit a change to the main branch. The ci will handle updating the lambda function. 

# Free account SNS Sand box Instructions
1. Need to add phone number to SNS Sand Box Destination Phone numbers

# Free Account Email Instructions
1. Verify email address with SES. 
2. You will also need to do this for all of your testing emails. 
# Issues
1. The main issues is I ran out of time to make the lambda function do exactly as the assignment required. I am confident with another few hours I could finish that. The remaining items left to implement would be:
 - Getting the s3 objects (A simple s3 call)
 - Sending a sms mesage via SNS to the phone number from the s3 object 
 - Sending an email via ses to the email address from the s3 object
 - Add the records to the dynamodb along with file details, times, execution status etc. 
 
 I spent the majority of my time on the IaC and CICD. I took the opportunity to learn the goformation library which is super neat if you haven't checked it out. 

2. Having to manually create the key for the upload user is a little tedious: would look to somehow automate this. Seems the goformation library  doesn't support the cloudformation Fn:GetAttr intrinsic function. Supporting automation of this via cloud formation would require injecting the config into the update function. 
3. There are no tests. 


# Features
1. The stack can be quickly created and deleted using IaC principles using awslabs' goformation library. 
2. I created my own ci pipeline in gitlab to compile and upload the lambda function to an s3 bucket, and update the lambda function. This required creating my own docker image and uploading it to the image registry for my repo. 
3. The majority of the code is in go! 

# TODO Items
 - Finish the Lambda Function to Send SNS, SES, and write to dynamodb. 
 - Implement Test and Associated Pipeline
 - Implement deadletter queue
 - Implement tagging and release deployments
 - Implement a development and production environments
 - Reduce the amount of manual work
