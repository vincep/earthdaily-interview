package main

import (
	"flag"
	"fmt"
	"main/formation"
)

func main() {

	action := flag.String("a", "", "The action to take")
	flag.Parse()

	switch *action {
	case "execute":
		// Creates the initial cloudformation stack
		formation.ExecuteStackCreation()
	case "update":
		// Updates the cloudformation stack
		formation.ExecuteStackUpdate()
	case "delete":
		// TODO: Implement delete the cloudformation stack
		formation.ExecuteStackDeletion()
	case "upload":
		// Runs the upload tests to the s3 bucket
		fmt.Println("upload hasn't been implemented")
	case "generate":
		// Prints the initial CloudFormation Template
		fmt.Printf("%s\n", string(formation.GenerateFormation()))
	default:
		fmt.Println("Please provide an action (-a execute|update|delete|generate)")
		return
	}

}
