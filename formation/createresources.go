package formation

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/aws/aws-sdk-go/service/cloudformation/cloudformationiface"
)

// MakeStack creates an AWS CloudFormation stack
// Inputs:
//     svc is an AWS CloudFormation service client
//     stackName is the name of the new stack
//     templateBody is the contents of the AWS CloudFormation template
// Output:
//     If success, nil
//     Otherwise, an error from the call to CreateStack
func makeStack(svc cloudformationiface.CloudFormationAPI, stackName *string, templateBody *string) error {
	_, err := svc.CreateStack(&cloudformation.CreateStackInput{
		TemplateBody: templateBody,
		StackName:    stackName,
		Capabilities: []*string{aws.String("CAPABILITY_NAMED_IAM")},
	})

	return err
}

func ExecuteStackCreation() {
	stackName := "earthdailyteststack"

	// Convert []byte to string
	templateBody := string(GenerateFormation())
	fmt.Printf("The execution function")
	fmt.Printf("%s\n", string(templateBody))

	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("ca-central-1"),
		Credentials: credentials.NewSharedCredentials("", "default"),
	})

	if err != nil {
		log.Fatal(err)
	}

	_, err = sess.Config.Credentials.Get()
	if err != nil {
		log.Fatal(err)
	}

	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc := cloudformation.New(sess)

	err = makeStack(svc, &stackName, &templateBody)
	if err != nil {
		fmt.Println("Got an error creating stack "+stackName+" using template from "+templateBody, err)
		return
	}

	err = svc.WaitUntilStackCreateComplete(&cloudformation.DescribeStacksInput{
		StackName: &stackName,
	})
	if err != nil {
		fmt.Println("Got an error waiting for stack to be created", err)
		return
	}

	fmt.Println("Created stack " + stackName + " using generated template.")

}
