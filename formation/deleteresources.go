package formation

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/aws/aws-sdk-go/service/cloudformation/cloudformationiface"
)

// RemoveStack deletes an AWS CloudFormation stack
// Inputs:
//     svc is an AWS CloudFormation service client
//     stackName is the name of the new stack
// Output:
//     If success, nil
//     Otherwise, an error from the call to DeleteStack
func removeStack(svc cloudformationiface.CloudFormationAPI, stackName *string) error {
	_, err := svc.DeleteStack(&cloudformation.DeleteStackInput{
		StackName: stackName,
	})

	return err
}

func ExecuteStackDeletion() {
	stackName := "earthdailyteststack"
	fmt.Printf("begining to delete stack %s", stackName)
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("ca-central-1"),
		Credentials: credentials.NewSharedCredentials("", "default"),
	})

	if err != nil {
		log.Fatal(err)
	}

	_, err = sess.Config.Credentials.Get()
	if err != nil {
		log.Fatal(err)
	}

	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc := cloudformation.New(sess)

	err = removeStack(svc, &stackName)
	if err != nil {
		fmt.Println("Got an error deleting stack "+stackName, err)
		return
	}

	err = svc.WaitUntilStackDeleteComplete(&cloudformation.DescribeStacksInput{
		StackName: &stackName,
	})
	if err != nil {
		fmt.Println("Got an error waiting for stack to be deleted", err)
		return
	}

	fmt.Println("Deleted stack " + stackName)
}
