package formation

import (
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/awslabs/goformation/v6/cloudformation"
	"github.com/awslabs/goformation/v6/cloudformation/dynamodb"
	"github.com/awslabs/goformation/v6/cloudformation/iam"
	"github.com/awslabs/goformation/v6/cloudformation/lambda"
	"github.com/awslabs/goformation/v6/cloudformation/policies"
	"github.com/awslabs/goformation/v6/cloudformation/s3"
	"github.com/awslabs/goformation/v6/cloudformation/tags"
)

// Generates the cloud formation template
func GenerateFormation() []byte {

	finalTemplate := cloudformation.NewTemplate()

	finalTemplate.Resources["s3Bucket"] = s3Bucket()
	finalTemplate.Resources["user"] = user()
	finalTemplate.Resources["dynamoDb"] = dynamoDb()

	j, err := finalTemplate.JSON()
	if err != nil {
		fmt.Printf("Failed to generate JSON: %s\n", err)
	}

	return j
}

// Generates the cloud formation update template
func UpdateFormation() []byte {

	// Add the AWS::Lambda::Permission to the s3 bucket
	// Add the User Permission to s3 bucket
	updateFormation := cloudformation.NewTemplate()
	updateFormation.Resources["userToGroup"] = userToGroup()
	updateFormation.Resources["s3Bucket"] = s3Bucket()
	updateFormation.Resources["dynamoDb"] = dynamoDb()
	updateFormation.Resources["user"] = user()
	// updateFormation.Resources["userKey"], updateFormation.Outputs = userKey()
	updateFormation.Resources["lambda"] = lambdaFun()

	j, err := updateFormation.JSON()
	if err != nil {
		fmt.Printf("Failed to generate JSON: %s\n", err)
	}

	return j
}

// Generates the s3 cloud formation template
func s3Bucket() cloudformation.Resource {

	template := cloudformation.NewTemplate()

	template.Resources["S3Bucket"] = &s3.Bucket{
		BucketName: aws.String("earthdaily-interview-test"),
		PublicAccessBlockConfiguration: &s3.Bucket_PublicAccessBlockConfiguration{
			BlockPublicAcls:                 aws.Bool(true),
			BlockPublicPolicy:               aws.Bool(true),
			IgnorePublicAcls:                aws.Bool(true),
			RestrictPublicBuckets:           aws.Bool(true),
			AWSCloudFormationDeletionPolicy: policies.DeletionPolicy("Retain"),
		},
		Tags: &[]tags.Tag{
			{
				Key:   "Name",
				Value: "earthdaily-interview-test",
			},
		},
	}

	return template.Resources["S3Bucket"]

}

// Generates the lambda cloud formation template
// TODO: Implement Deadletter Config
func lambdaFun() cloudformation.Resource {
	template := cloudformation.NewTemplate()

	template.Resources["Lambda"] = &lambda.Function{
		Code: &lambda.Function_Code{
			S3Bucket: aws.String("lambda-s3-earthdaily"),
			S3Key:    aws.String("main.zip"),
		},
		Description:  aws.String("Lambda Function for EarthDailyTesting"),
		FunctionName: aws.String("EarthDailyLambda"),
		Role:         "arn:aws:iam::185349814279:role/lambda1",
		Runtime:      aws.String("go1.x"),
		Tags: &[]tags.Tag{
			{
				Key:   "Name",
				Value: "earthdaily-interview-test",
			},
		},
		TracingConfig: &lambda.Function_TracingConfig{
			Mode: aws.String("Active"),
		},
		Handler: aws.String("mainlambda"),
	}

	return template.Resources["Lambda"]
}

// Generates the dynamoDb cloud formation template
// Inquire about message size... 400Kb limit/field.
func dynamoDb() cloudformation.Resource {

	template := cloudformation.NewTemplate()

	template.Resources["DynamoDb"] = &dynamodb.Table{
		TableName: aws.String("earthdailytesttable"),
		AttributeDefinitions: &[]dynamodb.Table_AttributeDefinition{
			{
				AttributeName: "UID",
				AttributeType: "S",
			},
		},
		KeySchema: []dynamodb.Table_KeySchema{
			{
				AttributeName: "UID",
				KeyType:       "HASH",
			},
		},
		Tags: &[]tags.Tag{
			{
				Key:   "Name",
				Value: "earthdaily-interview-test",
			},
		},
		ProvisionedThroughput: &dynamodb.Table_ProvisionedThroughput{
			ReadCapacityUnits:  5,
			WriteCapacityUnits: 5,
		},
	}

	return template.Resources["DynamoDb"]

}

// Generates the upload user formation template
func user() cloudformation.Resource {

	template := cloudformation.NewTemplate()

	template.Resources["User"] = &iam.User{
		Tags: &[]tags.Tag{
			{
				Key:   "Name",
				Value: "earthdaily-interview-test",
			},
		},
		UserName: aws.String("testuser1"),
	}

	return template.Resources["User"]

}

// function is not used since goformation doesn't currently support the Fn:GetAttr intrinsic function.
func userKey() (cloudformation.Resource, cloudformation.Outputs) {

	template := cloudformation.NewTemplate()

	template.Resources["UserKey"] = &iam.AccessKey{
		UserName: cloudformation.Ref("user"),
	}

	template.Outputs["AccessKeyforTestUser"] = cloudformation.Output{
		Value: "testuser1",
	}
	template.Outputs["SecretKeyforTetUser"] = cloudformation.Output{}

	return template.Resources["UserKey"], template.Outputs

}

// Adds the UserToGroup Cloudformation Template
func userToGroup() cloudformation.Resource {
	template := cloudformation.NewTemplate()

	template.Resources["UserToGroup"] = &iam.UserToGroupAddition{
		GroupName: "S3-Uploaders-Group",
		Users:     []string{"testuser1"},
	}

	return template.Resources["UserToGroup"]
}

// Generates the iam group cloud formation template
func group() cloudformation.Resource {
	template := cloudformation.NewTemplate()

	template.Resources["Group"] = &iam.Group{
		GroupName: aws.String("S3-Uploaders-Group"),
		Policies: &[]iam.Group_Policy{
			{
				PolicyDocument: `{
					"Version": "2012-10-17",
					"Statement": [
						{
							"Sid": "VisualEditor0",
							"Effect": "Allow",
							"Action": "s3:PutObject",
							"Resource": "arn:aws:s3:::*/*",
						}
					]
				}`,
				PolicyName: "s3-upload-only",
			},
		},
		AWSCloudFormationDeletionPolicy: policies.DeletionPolicy("Retain"),
	}

	return template.Resources["Group"]
}
