package formation

import (
	"fmt"
	"log"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/cloudformation"
	"github.com/aws/aws-sdk-go/service/cloudformation/cloudformationiface"
)

func updateStack(svc cloudformationiface.CloudFormationAPI, stackName *string, templatBody *string) error {
	_, err := svc.UpdateStack(&cloudformation.UpdateStackInput{
		StackName:    stackName,
		TemplateBody: templatBody,
		Capabilities: []*string{aws.String("CAPABILITY_NAMED_IAM")},
	})

	return err
}

func ExecuteStackUpdate() {
	stackName := "earthdailyteststack"
	fmt.Printf("begining updates to stack %s", stackName)

	templateBody := string(UpdateFormation())
	sess, err := session.NewSession(&aws.Config{
		Region:      aws.String("ca-central-1"),
		Credentials: credentials.NewSharedCredentials("", "default"),
	})

	if err != nil {
		log.Fatal(err)
	}

	_, err = sess.Config.Credentials.Get()
	if err != nil {
		log.Fatal(err)
	}

	sess = session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	svc := cloudformation.New(sess)

	err = updateStack(svc, &stackName, &templateBody)
	if err != nil {
		fmt.Println("Got an error updating stack "+stackName, err)
		return
	}

	err = svc.WaitUntilStackUpdateComplete(&cloudformation.DescribeStacksInput{
		StackName: &stackName,
	})
	if err != nil {
		fmt.Println("Got an error waiting for stack to be updated", err)
		return
	}

	fmt.Println("Updated stack " + stackName)
}
