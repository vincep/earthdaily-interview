module main

go 1.17

require (
	github.com/aws/aws-sdk-go v1.43.18
	github.com/awslabs/goformation/v6 v6.0.2
)

require (
	github.com/jmespath/go-jmespath v0.4.0 // indirect
	github.com/sanathkr/go-yaml v0.0.0-20170819195128-ed9d249f429b // indirect
	github.com/sanathkr/yaml v0.0.0-20170819201035-0056894fa522 // indirect
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f // indirect
)
